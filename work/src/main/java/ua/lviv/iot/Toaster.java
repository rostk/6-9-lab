package ua.lviv.iot;

public class Toaster extends KitchenAppliance {

    private float cookingTime;

    public Toaster(final double power, final String name, final String compatibility,
                   final DishesType dishes, final float cookingTime) {
        super(power, name, compatibility, dishes);
        this.cookingTime = cookingTime;
    }

    public final float getCookingTime() {
        return cookingTime;
    }

    public final void setCookingTime(final float cookingTime) {
        this.cookingTime = cookingTime;
    }

    public String getHeaders() {
        return "Name" + ": " + "               " + "Parameters :" + "\n " + "cookingTime : " + "      ";
    }

    public String toCSV() {
        return getCookingTime() + ";\n ";
    }

    @Override
    public final String toString() {
        return "ua.lviv.iot.Toaster: "
                + " CookingTime =\n "
                + cookingTime
                + " " + super.toString() + "\n";
    }
}
