package ua.lviv.iot;

public class Juicer extends KitchenAppliance {
    private double capacity;

    public Juicer(final double power, final String name, final String compatibility,
                  final DishesType dishes, final double capacity) {
        super(power, name, compatibility, dishes);
        this.capacity = capacity;
    }

    public final double getCapacity() {
        return capacity;
    }

    public final void setCapacity(final double capacity) {
        this.capacity = capacity;
    }

    public String getHeaders() {
        return "capacity : "+"                  ";
    }

    public String toCSV() {
        return getCapacity() + ";\n";
    }

    @Override
    public final String toString() {
        return "ua.lviv.iot.Juicer: "
                + "capacity = " + capacity
                + " " + super.toString() + "\n";
    }
}