package ua.lviv.iot;

public class main {
    public static void main(String[] args) {
        KitchenManager appliance = new KitchenManager();
        appliance.addAppliance(new Toaster(16.23, "ua.lviv.iot.Toaster", "Bread", DishesType.BREADPRODUCTS, 15));
        appliance.addAppliance(new Mixer(50.45, "ua.lviv.iot.Mixer", "Cakes", DishesType.CAKES, 13));
        appliance.addAppliance(new Juicer(100.32, "ua.lviv.iot.Juicer", "Juice", DishesType.JUICE, 12));
        appliance.addAppliance(new Oven(60.42, "ua.lviv.iot.Oven", "Bakery", DishesType.DESERTS, 90));

        KitchenApplianceWriter writer = new KitchenApplianceWriter();
        writer.writeToFile(appliance.getAppliance());


    }
}
