package ua.lviv.iot.persistance.Dao;
import ua.lviv.iot.KitchenAppliance;

public interface KitchenApplienceDao extends IDao<KitchenAppliance>  {
}
