package ua.lviv.iot.persistance.Dao;

import ua.lviv.iot.KitchenAppliance;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

public class KitchenApplienceDaoImpl extends AbstractDao<KitchenAppliance> implements KitchenApplienceDao,Serializable {
    private static final long serialVersionUID = 1L;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    protected Class<KitchenAppliance> getEntityClass() {
        return KitchenAppliance.class;
    }
}
