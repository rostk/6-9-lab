package ua.lviv.iot;

public enum DishesType {
    JUICE, BREADPRODUCTS, CAKES, DESERTS
}
