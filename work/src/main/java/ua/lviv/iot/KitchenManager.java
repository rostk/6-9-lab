package ua.lviv.iot;

import javax.enterprise.context.Dependent;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Dependent
public class KitchenManager {
    public KitchenManager() {
    }

    private List<KitchenAppliance> appliance = new LinkedList();

    private Map<Integer, KitchenAppliance> applianceMap;

    public final void setAppliance(final List<KitchenAppliance> appliance) {
        this.appliance = appliance;

    }


    public final void addAppliance(final KitchenAppliance appliance) {
        this.appliance.add(appliance);
    }


    public final List<KitchenAppliance> sortByPower(List<KitchenAppliance> appliance) {
        appliance.sort(Comparator.comparingDouble(KitchenAppliance::getPower));
        return appliance;
    }


    public final List<KitchenAppliance> findByCompatibility(final DishesType dishesType) {
        List<KitchenAppliance> result = new LinkedList<>();
        for (KitchenAppliance app : appliance) {
            if (dishesType == app.getDishes()) {
                result.add(app);
            }
        }
        return result;
    }


    public final List<KitchenAppliance> getAppliance() {
        return appliance;
    }


}
