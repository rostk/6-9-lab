package ua.lviv.iot;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Path("/appliance")
public class ApplianceService implements Serializable {

    @Inject
    private KitchenManager kitchenManager;

    private static Map<Integer, KitchenAppliance> applianceMap = new HashMap<>();
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public  KitchenAppliance getKitchenAppliance(@PathParam("id") Integer id){
        return applianceMap.get(id);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public  Response createKitchenAppliance(KitchenAppliance appliance) {
        applianceMap.put(appliance.getId(), appliance);
        return Response.status(200).entity("appliance").build();
    }
    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public  Response deleteKitchenAppliance(@PathParam("id") Integer id){
        applianceMap.remove(id);
        return Response.status(200).entity("appliance").build();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public  Response updateKitchenAppliance(KitchenAppliance appliance){
        applianceMap.put(appliance.getId(), appliance);
        return Response.status(200).entity("appliance").build();
    }
}
