package ua.lviv.iot;

import javax.persistence.*;

@Entity
public class KitchenAppliance {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;
    @Column
    private double power;
    @Column
    private String compatibility;
    @Column
    private DishesType dishes;
    @Column
    private String name;

    public KitchenAppliance() {
    }

    public final String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public KitchenAppliance(final double power, final String name,
                            final String compatibility, final DishesType dishes) {
        this.power = power;
        this.name = name;
        this.compatibility = compatibility;
        this.dishes = dishes;
    }

    public final String getCompatibility() {
        return compatibility;
    }

    public final DishesType getDishes() {
        return dishes;
    }

    public final double getPower() {
        return power;
    }

    public final void setPower(final double power) {
        this.power = power;
    }

    public void setCompatibility(String compatibility) {
        this.compatibility = compatibility;
    }

    public void setDishes(DishesType dishes) {
        this.dishes = dishes;
    }

    @Override
    public String toString() {
        return "Power = " + power
                + " Compatibility = "
                + compatibility
                + " Dishes = " + dishes
                + " name = " + name;
    }

    public String getHeaders() {
        return "power" + ", " + "name" + ", " + "compatibility" + ", " + "dishes,";
    }

    public String toCSV() {
        return getName() + ", " + getPower() + ", " + getCompatibility() + ", " + getDishes() + ", ";
    }
}