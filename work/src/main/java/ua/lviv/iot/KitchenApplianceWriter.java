package ua.lviv.iot;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class KitchenApplianceWriter {


    public void writeToFile(List<KitchenAppliance> appliance) {
        File csvText = new File("CSV text.csv");
        File headersText = new File("Headers text.csv");
        try {
            FileWriter fileWriterCsv = new FileWriter(csvText);

            //   FileWriter fileWriterHeaders = new FileWriter(headersText);
            for (KitchenAppliance kitchenAppliance : appliance) {
                fileWriterCsv.write(kitchenAppliance.getHeaders());
                fileWriterCsv.write(kitchenAppliance.toCSV());

            }

            fileWriterCsv.close();
            //fileWriterHeaders.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}