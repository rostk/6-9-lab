package ua.lviv.iot;

public class Mixer extends KitchenAppliance {
    private int rotate;

    public Mixer(final double power, final String name, final String compatibility,
                 final DishesType dishes, final int rotate) {
        super(power, name, compatibility, dishes);
        this.rotate = rotate;
    }

    public final int getRotate() {
        return rotate;
    }

    public final void setRotate(final int rotate) {
        this.rotate = rotate;
    }

    public String getHeaders() {
        return "rotate : " + "                     ";
    }

    public String toCSV() {
        return getRotate() + ";\n";
    }

    @Override
    public final String toString() {
        return "ua.lviv.iot.Mixer: "
                + "rotate = " + rotate
                + " " + super.toString() + "\n";
    }
}

