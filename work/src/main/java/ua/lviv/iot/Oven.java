package ua.lviv.iot;

public class Oven extends KitchenAppliance {
    private int temperature;


    public Oven(final double power, final String name, final String compatibility,
                final DishesType dishes, final int temperature) {
        super(power, name, compatibility, dishes);
        this.temperature = temperature;
    }

    public final int getTemperature() {
        return temperature;
    }

    public final void setTemperature(final int temperature) {
        this.temperature = temperature;
    }

    public String getHeaders() {
        return "temperature : "+"         ";
    }

    public String toCSV() {
        return getTemperature() + ". ";
    }

    @Override
    public final String toString() {
        return "ua.lviv.iot.Oven: "
                + "temperature = " + temperature
                + " " + super.toString() + "\n";
    }
}
