/*import org.junit.Before;
import org.junit.Test;
import ua.lviv.iot.*;

import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class KitchenManagerTest {
    KitchenManager kitchenManager = new KitchenManager();


    @Before
    public void setUp() throws Exception {
        kitchenManager.addAppliance(new Toaster(16.23, "ua.lviv.iot.Toaster",
                "Bread", DishesType.BREADPRODUCTS, 13));
        kitchenManager.addAppliance(new Mixer(50.45, "ua.lviv.iot.Mixer",
                "Cakes", DishesType.CAKES, 13));
        kitchenManager.addAppliance(new Juicer(100.32, "ua.lviv.iot.Juicer",
                "Juice", DishesType.JUICE, 12));
        kitchenManager.addAppliance(new Oven(60.42, "ua.lviv.iot.Oven",
                "Bakery", DishesType.CAKES, 90));
    }

    @Test
    public void testSortByPower() {
        List<KitchenAppliance> result = kitchenManager.sortByPower(kitchenManager.getAppliance());
        assertEquals(4, result.size());
        double powerOfThePreviousOneInList = 0;
        for (KitchenAppliance kitchenAppliance : result) {
            if (kitchenAppliance.getPower() > powerOfThePreviousOneInList) {
                powerOfThePreviousOneInList = kitchenAppliance.getPower();
                assertTrue(true);
            } else {
                assertTrue(false);
            }

        }
    }

    @Test
    public void findByCompatibility() {
        List<KitchenAppliance> result = kitchenManager.findByCompatibility(DishesType.CAKES);
        assertEquals(2, result.size());
        for (KitchenAppliance kitchenAppliance : result) {
            if (kitchenAppliance.getDishes() == DishesType.CAKES) {
                assertTrue(true);
            } else {
                assertTrue(false);
            }
        }
    }
}*/

