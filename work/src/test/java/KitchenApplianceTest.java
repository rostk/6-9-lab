/*import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;
import ua.lviv.iot.DishesType;
import ua.lviv.iot.KitchenAppliance;

import java.util.LinkedList;
import java.util.List;



public class KitchenApplianceTest {
    KitchenAppliance kitchenAppliance = new KitchenAppliance(54,"fdgdfg","dsfs", DishesType.CAKES);
    List<KitchenAppliance> appliance = new LinkedList<>();

    @Before
    public void setUp() throws Exception {
        appliance.add(new KitchenAppliance(100.32, "ua.lviv.iot.Juicer",
                "Juice", DishesType.JUICE));
      TestCase.assertEquals(100.32,appliance.get(0).getPower());
        TestCase.assertEquals("ua.lviv.iot.Juicer",appliance.get(0).getName());
        TestCase.assertEquals(DishesType.JUICE,appliance.get(0).getDishes());



    }

    @Test
    public void getName() {
        kitchenAppliance.setName("ua.lviv.iot.Juicer");
        TestCase.assertEquals("ua.lviv.iot.Juicer", kitchenAppliance.getName());
        kitchenAppliance.setName("ua.lviv.iot.Mixer");
        TestCase.assertEquals("ua.lviv.iot.Mixer", kitchenAppliance.getName());
        kitchenAppliance.setName("ua.lviv.iot.Oven");
        TestCase.assertEquals("ua.lviv.iot.Oven", kitchenAppliance.getName());
        kitchenAppliance.setName("ua.lviv.iot.Toaster");
        TestCase.assertEquals("ua.lviv.iot.Toaster", kitchenAppliance.getName());
    }

    @Test
    public void setName() {
        kitchenAppliance.setName("ua.lviv.iot.Juicer");
        TestCase.assertEquals("ua.lviv.iot.Juicer", kitchenAppliance.getName());
        kitchenAppliance.setName("ua.lviv.iot.Mixer");
        TestCase.assertEquals("ua.lviv.iot.Mixer", kitchenAppliance.getName());
        kitchenAppliance.setName("ua.lviv.iot.Oven");
        TestCase.assertEquals("ua.lviv.iot.Oven", kitchenAppliance.getName());
        kitchenAppliance.setName("ua.lviv.iot.Toaster");
        TestCase.assertEquals("ua.lviv.iot.Toaster", kitchenAppliance.getName());
    }


    @Test
    public void getPower() {
        kitchenAppliance.setPower(100.32);
        TestCase.assertEquals(100.32, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(50.45);
        TestCase.assertEquals( 50.45, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower( 60.42);
        TestCase.assertEquals(60.42, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(16.23);
        TestCase.assertEquals(16.23, kitchenAppliance.getPower(),0.00000000001);
    }

    @Test
    public void setPower() {
        kitchenAppliance.setPower(100.32);
        TestCase.assertEquals(100.32, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(50.45);
        TestCase.assertEquals( 50.45, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower( 60.42);
        TestCase.assertEquals(60.42, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(16.23);
        TestCase.assertEquals(16.23, kitchenAppliance.getPower(),0.00000000001);
    }


}*/